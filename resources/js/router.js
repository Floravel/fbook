import Vue from 'vue';
import VueRouter from 'vue-router'
import Start from "./views/Start";
import NewsFeed from "./views/NewsFeed";

/* use vou-router as a plugin for Vue */

Vue.use(VueRouter);

export default new VueRouter({
    mode: 'history',

    routes: [
        {
            path: '/', name: 'home', component: NewsFeed,
        }
    ]
});
